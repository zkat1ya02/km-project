﻿// ConsoleApplication3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <WS2tcpip.h>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <stdio.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <io.h>

#pragma comment (lib, "ws2_32.lib")


using namespace std;
int main()
{
    char CommandBuf[250];
    string command;
    ofstream fout;
    ofstream sysout;
    char UknonwnResponse[1024] = "Unknown command. Available commands are Who, Disconnect, Transport";
    char WhoResponse[1024] = "Variant 2: Text file transporting\n This lab was done by Zubenko Kateryna";
    char data[1024];
    char filename[12];
    char filesize_str[1024];
    int filesize;
    int receivedBytes = 0;
    chrono::duration <double, milli> diff;
    struct sockaddr_in server_addr;
    // Initialze winsock
    WSADATA wsData;
    WORD ver = MAKEWORD(2, 2);

    int wsOk = WSAStartup(ver, &wsData);
    if (wsOk != 0)
    {
        cerr << "Can't Initialize winsock! Quitting" << endl;
        return 0;
    }

    // Create a socket
    SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == INVALID_SOCKET)
    {
        cerr << "Can't create a socket! Quitting" << endl;
        return 0;
    }

    // Bind the ip address and port to a socket
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(54000);
    hint.sin_addr.S_un.S_addr = INADDR_ANY; // Could also use inet_pton .... 

    bind(listening, (sockaddr*)&hint, sizeof(hint));

    // Tell Winsock the socket is for listening 
    listen(listening, SOMAXCONN);

    // Wait for a connection
    sockaddr_in client;
    int clientSize = sizeof(client);

    SOCKET clientSocket = accept(listening, (sockaddr*)&client, &clientSize);

    char host[NI_MAXHOST];    // Client's remote name
    char service[NI_MAXSERV];  // Service (i.e. port) the client is connect on

    ZeroMemory(host, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
    ZeroMemory(service, NI_MAXSERV);

    if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
    {
        cout << host << " connected on port " << service << endl;
    }
    else
    {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        cout << host << " connected on port " <<
            ntohs(client.sin_port) << endl;
    }
    // While loop: accept and echo message back to client
    char buf[4096];
    sysout.open("res.txt");
    while (1) {
        int recvStatus = recv(clientSocket, CommandBuf, 250, 0);
        command = CommandBuf;
        std::cout << "\nrecvStatus: " << recvStatus << std::endl;
        sysout << " Command [" << command << "] was accepted from client" << endl;
        if (command == "Who") {
            send(clientSocket, WhoResponse, 1024, 0);
        }
        else if (command == "Disconnect") {
            cout << "Closing the connection" << endl;
            break;
        }
        else if (command == "Transport") {
            recv(clientSocket, filename, 12, 0);
            recv(clientSocket, filesize_str, 1024, 0);
            stringstream strValue;
            strValue << filesize_str;
            strValue >> filesize;
            fout.open(filename);
            while (1) {
                auto start = chrono::high_resolution_clock::now();
                recv(clientSocket, data, 1024, 0);
                auto end = chrono::high_resolution_clock::now();
                if (receivedBytes >= filesize) {
                    break;
                    cout << "File transported succesfully" << endl;
                }
                diff = end - start;
                receivedBytes += 1024;
                cout << "here3" << endl;
                cout << "Recieved: " << receivedBytes << "bytes" << endl;
                cout << "Current speed: " << receivedBytes / 1000 << " Bytes/second" << endl;
                fout.write(data, 1024);
            }
            fout.close();
        }
        else {
            send(clientSocket, UknonwnResponse, 1024, 0);
        }
    }

    // Close the socket
    closesocket(clientSocket);

    // Cleanup winsock
    WSACleanup();

    system("pause");
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
