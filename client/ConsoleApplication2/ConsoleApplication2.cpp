﻿// ConsoleApplication2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <stdio.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <io.h>
#include <stdlib.h>
#include <cmath>
#include <WS2tcpip.h>
#include <iostream>
#include <chrono>
#include <ctime>   
#pragma comment(lib, "ws2_32.lib")

using namespace std;

int main()
{
    string ipAddress = "127.0.0.1";      // IP Address of the server
    int port = 1027;
    ifstream fin;
    ofstream sysout;
    char CommandBuf[250];
    char Response[1024];
    char filename[12] = "send.txt";
    char info[1024];
    int sentBytes = 0;
    const char* filesize_str;
    chrono::duration <double, milli> diff;
    chrono::duration <double, milli> diff_for_whole_op;
    string command;
    // Listening port # on the server

    // Initialize WinSock
    WSAData data;
    WORD ver = MAKEWORD(2, 2);
    int wsResult = WSAStartup(ver, &data);
    if (wsResult != 0)
    {
        cerr << "Can't start Winsock, Err #" << wsResult << endl;
        return 0;
    }

    // Create socket
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == INVALID_SOCKET)
    {
        cerr << "Can't create socket, Err #" << WSAGetLastError() << endl;
        WSACleanup();
        return 0;
    }

    // Fill in a hint structure
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);

    // Connect to server
    int connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
    if (connResult == SOCKET_ERROR)
    {
        cerr << "Can't connect to server, Err #" << WSAGetLastError() << endl;
        closesocket(sock);
        WSACleanup();
        return 0;
    }
    cout << "Client connected to server" << endl;

    // Do-while loop to send and receive data
    char buf[4096];
    string userInput;

    sysout.open("journal.txt");
    if (sysout.fail()) {
        std::cout << "can not open" << std::endl;
    }
    else {
        std::cout << "file is open" << std::endl;
    }


    while (1) {
        cout << "Enter command: ";
        cin >> CommandBuf;
        command = CommandBuf;
        int sendStatus = send(sock, command.c_str(), command.size() + 1, 0);
        std::cout << "\nsendStatus: " << sendStatus << std::endl;
        std::cout << " Command [" << command << "] was sent to the server" << std::endl;
        sysout << " Command [" << command << "] was sent to the server" << endl;
        if (command == "Disconnect") {
            send(sock, CommandBuf, 250, 0);
           // _close(sock);
            break;
        }
        else if (command == "Transport") {
            send(sock, filename, 12, 0);
            fin.open(filename);
            fin.seekg(0, ios::end);
            int filesize = fin.tellg();
            fin.seekg(0);
            std::string s = to_string(filesize);
            //filesize_str = to_string(filesize).c_str();
            send(sock, s.c_str(), 1024, 0);
            auto whole_op_start = chrono::high_resolution_clock::now();
            while (sentBytes <= filesize) {
                fin.read(info, 1024);
                auto start = chrono::high_resolution_clock::now();
                send(sock, info, 1024, 0);
                auto end = chrono::high_resolution_clock::now();
                diff = end - start;
                sentBytes += 1024;
                cout << "Sent: " << sentBytes << "bytes" << endl;
                cout << "Current speed: " << 1024 / (diff.count() / 1000) << " Bytes/second" << endl;
                cout << floor(((float)sentBytes / filesize) * 100) << "% " << "completed" << endl;
      
            }
            auto whole_op_end = chrono::high_resolution_clock::now();
            diff_for_whole_op = whole_op_end - whole_op_start;
            cout << "File transported succesfully" << endl;
            cout << "The entire operation took " << diff_for_whole_op.count() << " miliseconds" << endl;
            fin.close();
        }
        else {
            recv(sock, Response, 1024, 0);
            cout << Response << endl;
        }
    }

    // Gracefully close down everything
    closesocket(sock);
    WSACleanup();
    return 0;
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
